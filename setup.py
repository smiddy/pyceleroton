from setuptools import setup, find_packages
setup(
      name="pyceleroton",
      description="An API for the celeroton frequency converters.",
      version="0.2.0",
      platforms=["win-amd64", 'win32'],
      author="Markus J Schmidt",
      author_email='schmidt@ifd.mavt.ethz.ch',
      license="GNU GPLv3+",
      url="https://gitlab.ethz.ch/ifd-lab/device-driver/pyceleroton",
      packages=find_packages(),
      install_requires=['pyserial>=3.4']
      )
