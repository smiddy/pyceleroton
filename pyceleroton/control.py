import warnings
import serial
import struct
import time
import logging
import threading

# create logger
logger = logging.getLogger(__name__)


class CC75500Error(Exception):
    """
    CC75500 exception handle
    """

    def __init__(self, code, message):
        self.logger = logging.getLogger(__name__)
        self.errStr = 'Error with code {}: {}'.format(code, message)
        self.logger.error(self.errStr)
        return

    def __str__(self):
        return self.errStr


class CC75500(serial.Serial):
    """Class for the celeroton CC75-500 frequency converter.

    The class
    CC75500 is used for frequency converters of the CC75-500
    series. pyserial is applied for serial communication with
    the controller. Furthermore, threading is used for the
    monitoring.

    :requires: pyserial (>=2.7)

    :param serport: Serial port of controller
    :type serport: int or str

    *Example*::

        ctCC75_400 = CC75500('COM10')
        # Set up a temperature monitoring
        monitVar = "temperature"
        threshold = 60
        ctCC75_400.monitor(monitVar, threshold)
        time.sleep(2)
        print(ctCC75_400.thread.is_alive())
        # Get the reference motor speed
        wantedVar = "reference speed"
        result = ctCC75_400.readValue(wantedVar)
        print("Motor will start with", result, "rpm.")
        ctCC75_400.start()
        time.sleep(3)
        ctCC75_400.stop()
        ctCC75_400.close()

    """

    def __init__(self, serport):
        """
        Constructor
        """
        # Import the existing constructor
        super().__init__(serport, baudrate=57600, stopbits=serial.STOPBITS_ONE,
                         bytesize=serial.EIGHTBITS, timeout=1, parity=serial.PARITY_NONE)
        self.logger = logging.getLogger(__name__)
        self.reset_input_buffer()
        # dictionary for error code 1
        self.errDict1 = {int('00000001', 2): 'Input overvoltage',
                         int('00000010', 2): 'Input undervoltage',
                         int('00000100', 2): 'Phase voltage limit',
                         int('00001000', 2): 'Phase current limit',
                         int('00010000', 2): 'Power limit',
                         int('00100000', 2): 'Overspeed',
                         int('01000000', 2): 'Converter OT',
                         int('10000000', 2): 'Motor OT'
                         }
        # dictionary for error code 2
        self.errDict2 = {int('0000000000000000000001000', 2): 'No motor',
                         int('0000000000000000000010000', 2): 'Phase U missing',
                         int('0000000000000000000100000', 2): 'Phase V missing',
                         int('0000000000000000001000000', 2): 'Phase W missing',
                         int('0000000000000000010000000', 2): 'Stall(1)',
                         int('0000000000000000100000000', 2): 'Stall(2)',
                         int('0000000000000100000000000', 2): 'CAN timeout',
                         int('0000000010000000000000000', 2): 'ExtBrd error',
                         int('0000000100000000000000000', 2): 'Enable not set',
                         int('0000001000000000000000000', 2): 'Invalid hall sig',
                         int('0001000000000000000000000', 2): 'DC current trip',
                         int('0010000000000000000000000', 2): 'Inv current trip',
                         int('0100000000000000000000000', 2): 'DC voltage trip',
                         int('1000000000000000000000000', 2): 'Input voltage trip'
                         }
        # legacy dictionary for error messages
        self.errDict = {int('0000', 16): 'OK',
                        int('4001', 16): 'Unknown command',
                        int('4002', 16): 'Wrong checksum',
                        int('4004', 16): 'Invalid format',
                        int('4008', 16): 'Readonly',
                        int('4010', 16): 'Type mismatch',
                        int('4020', 16): 'Unknown variable',
                        }

        # dictionary for status messages
        self.statusDict = {int('0000', 16): 'OK',
                           int('0008', 16): 'Overtemperature',
                           int('0010', 16): 'Overvoltage',
                           int('0020', 16): 'Undervoltage',
                           int('0040', 16): 'Stall 1',
                           int('0080', 16): 'Stall 2',
                           int('0100', 16): 'Stall 3',
                           int('0200', 16): 'Overspeed',
                           int('4000', 16): 'Enable',
                           int('8000', 16): 'Motor overtemperature'
                           }
        # dictionary for the variables, not all values included
        self.varDict = {'Speed reference': b'\x00',  # rpm
                        'Actual speed': b'\x01',  # rpm
                        'DC-Link current': b'\x02',  # mA
                        'DC-Link current reference': b'\x03',  # mA
                        'Converter temperature': b'\x04',  # Celsius
                        'DC-Link Voltage': b'\x05',  # V
                        'Output Power': b'\x06',  # W
                        'Motor temperature (THC)': b'\x07',  # Celsius
                        'Motor temperature (PTC)': b'\x08',  # Celsius
                        'Pole pairs': b'\x09',  # number
                        'Max. phase current': b'\x0A',  # mA
                        'Max. rotational speed': b'\x0B',  # rpm
                        'Synchronization current': b'\x0C',  # mA
                        'Axial moment of inertia': b'\x0D',  # kgm**2
                        'PM flux linkage': b'\x0E',  # Vs
                        'Phase inductance': b'\x0F',  # H
                        'Phase resistance': b'\x10',  # Ohm
                        'Rotation direction': b'\x11',  # 0/1
                        'Acc. Ratio (above sync.)': b'\x12',  # rpm/s
                        'Acc. Ratio (below sync.)': b'\x13',  # rpm/s
                        'Speed controller rise time': b'\x14',  # s
                        'User defined sync. Speed': b'\x15',  # rpm
                        'Default sync speed (lower)': b'\x16',  # rpm
                        'Default sync speed (upper)': b'\x17',  # rpm
                        'User def. sync speed (lower)': b'\x18',  # rpm
                        'User def. sync speed (upper)': b'\x19',  # rpm
                        'User defined control parameter': b'\x1A',  # 0/1
                        'Proportional speed gain': b'\x1B',  # -
                        'Integral speed gain': b'\x1C',  # -
                        }
        # dict for the variable types
        # Int16 = 1, UInt16 = 2, Int32 = 3, UInt32 = 4, Float = 5
        self.varTypeDict = {'Speed reference': 3,  # rpm
                            'Actual speed': 3,  # rpm
                            'DC-Link current': 1,  # mA
                            'DC-Link current reference': 1,  # mA
                            'Converter temperature': 1,  # Celsius
                            'DC-Link Voltage': 1,  # V
                            'Output Power': 1,  # W
                            'Motor temperature (THC)': 1,  # Celsius
                            'Motor temperature (PTC)': 1,  # Celsius
                            'Pole pairs': 2,  # number
                            'Max. phase current': 1,  # mA
                            'Max. rotational speed': 3,  # rpm
                            'Synchronization current': 2,  # mA
                            'Axial moment of inertia': 5,  # kgm**2
                            'PM flux linkage': 5,  # Vs
                            'Phase inductance': 5,  # H
                            'Phase resistance': 5,  # Ohm
                            'Rotation direction': 2,  # 0/1
                            'Acc. Ratio (above sync.)': 5,  # rpm/s
                            'Acc. Ratio (below sync.)': 5,  # rpm/s
                            'Speed controller rise time': 5,  # s
                            'User defined sync. Speed': 2,  # rpm
                            'Default sync speed (lower)': 3,  # rpm
                            'Default sync speed (upper)': 3,  # rpm
                            'User def. sync speed (lower)': 3,  # rpm
                            'User def. sync speed (upper)': 3,  # rpm
                            'User defined control parameter': 2,  # 0/1
                            'Proportional speed gain': 5,  # -
                            'Integral speed gain': 5,  # -
                            }
        self.thread = None
        # Get status of the motor. In case of error try to solve it
        codes = self.getstatus()
        if any(codes[:2]):
            warnstr = "During initialization, an error with codes {} and {} occured. " \
                      "Trying to acknowledge".format(codes[0], codes[1])
            self.logger.warning(warnstr)
            warnings.warn(warnstr)
            self.ackerror(codes[0], codes[1])
            self.getstatus(True)
        return

    def start(self):
        """Starts the motor.
        """
        self._sendrcv(b'\x02')
        self.logger.info("Motor started.")
        return

    def stop(self):
        """Stops the motor.
        """
        self._sendrcv(b'\x03')
        self.logger.info("Motor stopped.")
        return

    def getstatus(self, raiseerror: bool = False):
        """ Get the status of the motor. An CC75500Error is raised in case of an raiseerror.

        :param raiseerror: if set true, a CC75500Error
        :type raiseerror: bool
        :return: int array containing [errcode1, errcode2, warncode, infocode]
        """
        statusbyte = b'\x00'
        answer = self._sendrcv(statusbyte)
        errcode1 = int.from_bytes(answer[1:5], 'little')
        errcode2 = int.from_bytes(answer[5:9], 'little')
        warncode = int.from_bytes(answer[9:13], 'little')
        infocode = int.from_bytes(answer[13:17], 'little')
        if not infocode == 0:
            self.logger.info('Info message with code {}: {}'.format(infocode, self.errDict1[infocode]))
            print('Info message with code {}: {}'.format(infocode, self.errDict1[infocode]))
        if not warncode == 0:
            self.logger.warning('Warning with code {}: {}'.format(warncode, self.errDict1[warncode]))
            warnings.warn('Warning with code {}: {}'.format(warncode, self.errDict1[warncode]))
        if not errcode1 == 0:
            self.logger.error('Error with code {}: {}'.format(errcode1, self.errDict1[errcode1]))
            if raiseerror:
                raise CC75500Error(errcode1, self.errDict1[errcode1])
        if not errcode2 == 0:
            self.logger.error('Error with code {}: {}'.format(errcode2, self.errDict2[errcode2]))
            if raiseerror:
                raise CC75500Error(errcode2, self.errDict2[errcode2])
        return [errcode1, errcode2, warncode, infocode]

    def ackerror(self, errorcode1, errorcode2):
        """Acknowledge an error

        The method takes the answered statusCode of the controller and the
        already found statusStr. The user is asked to acknowledge the error
        and, if answered with yes, the error is acknowledged.

        :param errorcode1: error code 1 from getstatus
        :type errorcode1: int
        :param errorcode2: error code 2 from getstatus
        :type errorcode2: int
        """
        errhex1 = struct.pack('I', errorcode1)
        errhex2 = struct.pack('I', errorcode2)
        data = b'\x01' + errhex1 + errhex2
        self._sendrcv(data)
        self.logger.info('Acknowledges errors with codes {} and {}.'.format(errorcode1, errorcode2))
        return

    def readvalue(self, varname):
        """Read a selected value.

        The method reads the value of varname. Currently implemented:

        * "reference speed" in rpm
        * "actual speed" in rpm
        * "temperature" in Celsius

        :param varname: Desired value
        :type varname: str
        :returns varvalue: Actual value
        :rtype varvalue: int
        """
        varflag = int.from_bytes(self.varDict[varname], 'little')
        check_int = self.checksum((3, 4, varflag))
        read_command = struct.pack('<BBBB', 3, 4, varflag, check_int)
        self.write(read_command)
        answer = self.read(8)
        var_type = answer[2]
        if (7 != answer[0]) or (4 != answer[1]):
            raise RuntimeError("Cannot interpret answer.")
        # Get the value according to its variable type
        if 1 == var_type:
            answer_int = struct.unpack('<BBBiB', answer)
        elif 2 == var_type:
            answer_int = struct.unpack('<BBBIB', answer)
        elif 3 == var_type:
            answer_int = struct.unpack('<BBBiB', answer)
        elif 4 == var_type:
            answer_int = struct.unpack('<BBBIB', answer)
        elif 5 == var_type:
            answer_int = struct.unpack('<BBBfB', answer)
        else:
            raise ValueError("Cannot interpret answer.")
        var_value = answer_int[3]
        self.logger.debug("Read value {}. Is {}".format(varname, var_value))
        return var_value

    def writevalue(self, varname, varvalue):
        """Write a selected value.

        The method writes the varValue of varname. Currently implemented:

        * "reference speed" in rpm
        * "actual speed" in rpm
        * "temperature" in Celsius

        :param varname: Desired value
        :type varname: str
        :param varvalue: Actual value
        :type varvalue: int
        """
        var_flag = int.from_bytes(self.varDict[varname], 'little')
        var_type = self.varTypeDict[varname]
        write_int = (8, 5, var_flag, var_type, varvalue)
        if 1 == var_type:
            write_com = struct.pack('<BBBBh', *write_int)
        elif 2 == var_type:
            write_com = struct.pack('<BBBBH', *write_int)
        elif 3 == var_type:
            write_com = struct.pack('<BBBBi', *write_int)
        elif 4 == var_type:
            write_com = struct.pack('<BBBBI', *write_int)
        elif 5 == var_type:
            write_com = struct.pack('<BBBBf', *write_int)
        else:
            raise ValueError("Cannot interpret answer.")
        check_int = self.checksum(write_com)
        write_com += struct.pack('<B', check_int)
        self.write(write_com)
        answer = self.read(3)
        if (2 != answer[0]) or (5 != answer[1]) or (int('f9', 16) != answer[2]):
            raise RuntimeError("Cannot interpret answer.")
        self.logger.debug("Write value {}. Is now {}".format(varname, varvalue))
        return

    def errcheck(self, answer):
        """Takes the answer of the controller and raises error.

        :param answer: Answer of the controller
        :type answer: bytes
        """
        err_int = struct.unpack('<bbhhb', answer)
        try:
            err_string = self.errDict[err_int[2]]
            raise RuntimeError(err_string)
        except KeyError:
            raise ValueError('Unknown error code.')

    def reset(self):
        """Resets the controller.

        Because of invalid commands the controller can remain in a
        faulty state. The method sends 17  zero bits to the controller to
        clear the command status.
        """
        reset_byte = (00000000000000000).to_bytes(17, byteorder='big')
        self.write(reset_byte)
        answer = self.read()
        if b'' is not answer:
            raise RuntimeError("Cannot reset controller.")
        return

    @staticmethod
    def checksum(command):
        """Computes the checksum

        For a given command, which is intended to be sent to the controller,
        the method computes the checksum

        :param command: Command for the controller
        :type command: tuple/list of int, bytes
        :returns checksum: String of the status (for user)
        :rtype checksum: int
        """
        if tuple == type(command) or list == type(command):
            command_sum = sum(command)
            checksum = (~command_sum + 1) & 0xFF
        elif bytes == type(command):
            command_sum = sum(list(command))
            checksum = (~command_sum + 1) & 0xFF
        else:
            raise TypeError('Command must be int or bytes.')
        return checksum

    def monitor(self, varname, threshold):
        """Monitor a variable and stops motor on threshold.

        The method sets up a threading class object and monitors varname until
        the thread is closed. In case of threspassing the threshold, the motor
        is stopped and a runtime error is raised. The function creates a Thread
        instance of class threading.

        :param varname: Name of variable to be monitored
        :type varname: str
        :param threshold: Threshold for value
        :type threshold: int
        :returns self.thread: Running monitor thread
        :rtype self.thread: threading.Thread
        """

        def mon_thread(var_name, mon_threshold):
            """Local function for threading
            """
            while True:
                var_value = self.readvalue(var_name)
                if mon_threshold >= var_value:
                    time.sleep(5)
                else:
                    self.stop()
                    raise RuntimeError('Threshold exceeded. Motor stopped.')

        self.thread = threading.Thread(target=mon_thread,
                                       args=(varname, threshold))
        self.thread.daemon = True
        self.thread.start()
        return

    def _sendrcv(self, data):
        """ send the command to device and returns data and checksum"""
        lengthwrite = len(data) + 1
        checksumwrite = (~(lengthwrite + sum(data)) + 1) & 0xff
        writebyte = struct.pack('B', lengthwrite) + data + struct.pack('B', checksumwrite)
        self.write(writebyte)
        length = struct.unpack('b', self.read())[0]
        answer = self.read(length)
        checksum = answer[-1]
        chck = (~(length + sum(answer[:-1])) + 1) & 0xff
        if not checksum == chck:
            self.logger.error('Checksum does not match received package.')
            raise ValueError('Checksum does not match received package.')
        # Check if command is send back
        if not data[0] == answer[0]:
            self.logger.error('Command does not come back.')
            raise CC75500Error(-1, 'Command does not come back.')
        return answer

    def __exit__(self, exc_type, exc_value, traceback):
        self.stop()
        super().__exit__()
