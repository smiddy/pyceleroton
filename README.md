# pyceleroton
API for celeroton frequency converters and motors. An example can be found in `example.py`.

*Requires*: pyserial (>=3.4)

Tested with Anaconda 1.7.2 and Python 3.7 on Windows 10.

## Installation
First, install a version of pyserial in your environment. For pip use

```python
python -m pip install pyserial
```
or for conda
```python
conda install pyserial
```
Afterwards install `pyceleroton` from the project folder with
```python
python setup.py install
```
## class cc75500
The class
cc75500 is used for frequency converters of the CC75-500
converted. pyserial is applied for serial communication with
the controller. Furthermore, threading is used for the
monitoring.

## class cc75500Error
Error classs for cc75500