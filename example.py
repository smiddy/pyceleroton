import logging
import time
from pyceleroton import CC75500

if __name__ == '__main__':
    logging.basicConfig(filename="DEBUG_celeroton.log",
                        level=logging.DEBUG,
                        format='%(asctime)s - %(name)s - %(levelname)s'
                        ' - %(message)s')
    with CC75500('COM2') as ctCC75_400:
        codes = ctCC75_400.getstatus()
        ctCC75_400.ackerror(codes[0], codes[1])
        wantedVar = "Speed reference"
        result = ctCC75_400.readvalue(wantedVar)
        ctCC75_400.writevalue(wantedVar, 500)
        print("Motor will start with", result, "rpm.")
        ctCC75_400.start()
        time.sleep(1)
        print("Current speed", ctCC75_400.readvalue('Actual speed'), "rpm.")
        time.sleep(1)
        ctCC75_400.stop()
